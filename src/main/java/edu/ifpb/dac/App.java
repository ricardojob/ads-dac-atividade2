package edu.ifpb.dac;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Ricardo Job
 */
public class App {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("configurcao");
        EntityManager em = emf.createEntityManager();

        Pessoa pessoa = new Pessoa("job", 25, "123456");
        Pessoa segundaPessoa = new Pessoa("Chaves", 14, "123");
        
//        persist(em, pessoa);
//        persist(em, segundaPessoa);
        
        listar(em);
    }

    public static void persist(EntityManager em, Pessoa object) {

        try {
            em.getTransaction().begin();
            
            em.persist(object);

            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
    }

    private static void listar(EntityManager em) {
        
        List<Pessoa> lista = em.createQuery("Select p From Pessoa p").getResultList();
        
        for (Pessoa pessoa : lista) {
            System.out.println(pessoa);
        }
    }

}
